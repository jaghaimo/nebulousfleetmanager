import functools
import random
from math import ceil
import string
from .io import ship_component_data, components_by_slot


class ShipComponent:

    def __init__(self, name: str) -> None:
        self.name = name
        comp: dict = ship_component_data[name]
        self.slot: str = comp['slot']
        self.category: str = comp.get('category')
        self.type: str = comp['type']
        self.cost: float = float(comp['cost'])
        self.size: tuple[int, int, int] = comp['size']
        self.crew: int = comp.get('crew', 0)
        self.power: int = comp.get('power', 0)
        self.ammo: str = comp.get('ammo')
        self.cells: int = comp.get('cells')
        self.capacity: int = comp.get('capacity')
        self.crew_provided: int = comp.get('crewprovided', 0)
        self.power_provided: int = comp.get('powerprovided', 0)
        self.power_mult: float = comp.get('powermult')
        self.xsi_type: str = comp.get('xsi:type')
    
    def __str__(self) -> str:
        return self.name
    
    # @staticmethod
    # def lookup_type(type: str):
    #     pass

    # @staticmethod
    # def dps(weapon, ammo) -> float:
    #     if type(weapon) is str:
    #         weapon = ShipComponent(weapon)
    #     elif type(weapon) is not ShipComponent:
    #         weapon = None
    #     if type(ammo) is str:
    #         ammo = ShipComponent(ammo)
    #     elif type(ammo) is not ShipComponent:
    #         ammo = None

    #     if not weapon and not ammo:
    #         raise Exception("Incorrect object types provided for weapon and ammo.")
    #     elif not weapon:
    #         raise Exception("Incorrect object type provided for weapon.")
    #     elif not ammo:
    #         raise Exception("Incorrect object type provided for ammo.")
        
    #     avg_fire_rate = 60*(a:=weapon.get('autoloader'))/(weapon.get('recycle')*(a-1) + weapon.get('reload'))
    #     return avg_fire_rate * ammo.get('damage')


@functools.total_ordering
class ShipSocket:

    def __init__(self, socketdict: dict, key: str, default: bool = True) -> None:
        self.name: str = socketdict['name']
        self.key: str = key
        self.slot: str = self.name.split()[0].lower()
        self.size: tuple = socketdict['size']
        self._component: ShipComponent = None
        self._reset()
        if default and (d:=socketdict.get('default')):
            self.component = ShipComponent(d)

    def __lt__(self, other):
        try:
            self_cost = self.multiple*self.component.cost
        except:
            self_cost = 0
        try:
            other_cost = other.multiple*other.component.cost
        except:
            other_cost = 0
        return self_cost < other_cost

    def __eq__(self, other):
        try:
            self_cost = self.multiple*self.component.cost
        except:
            self_cost = 0
        try:
            other_cost = other.multiple*other.component.cost
        except:
            other_cost = 0
        return self_cost == other_cost
    
    def __str__(self) -> str:
        return self.name
        
    def cost(self) -> int:
        _cost = self._basecost()
        if self.stored_ammo:
            _cost += sum([a['cost'] for a in self.stored_ammo.values()])
        return _cost

    def crew(self) -> int:
        try:
            return self.component.crew
        except:
            return 0

    def crew_provided(self) -> int:
        try:
            return self.multiple*self.component.crew_provided
        except:
            return 0
    
    def power(self) -> int:
        try:
            return self.component.power
        except:
            return 0

    def power_provided(self) -> int:
        try:
            return self.component.power_provided
        except:
            return 0

    def power_mult(self) -> float:
        try:
            return self._power_compounding*self.component.power_mult
        except:
            return 0
    
    def _basecost(self) -> int:
        try:
            return int(self._compounding*self.component.cost)
        except:
            return 0

    def store_ammo(self, ammo: str, quantity: int) -> int:
        quantity = int(quantity)
        _ammo = ship_component_data[ammo]
        try:
            ac = _ammo['cost']
        except:
            raise Exception("Invalid ammo type.")
        if type(ac) is str:
            ac = eval(ac)
        if quantity:
            k = ''.join(random.choices(f'{string.ascii_letters}-_', k = 22))
            if self.component.cells and self.component.ammo == _ammo['type']:
                freecells = self.component.cells \
                    - sum(c['quantity'] for c in self.stored_ammo.values())
                if a:=self.stored_ammo.get(ammo):
                    freecells += a['quantity']
                if q:=min(quantity, freecells):
                    self.stored_ammo[ammo] = {
                        'quantity': q,
                        'cost': q*ac,
                        'magazine_key': k
                    }
                return q
            elif self._type == "Magazine":
                freecapacity = self.multiple*self.component.capacity - \
                    sum([v['volume'] for v in self.stored_ammo.values()])
                if a:=self.stored_ammo.get(ammo):
                    freecapacity += a['volume']
                # Getting around floating point error
                freecapacity += 0.001
                if q:=min(quantity, int(freecapacity//_ammo['volume'])):
                    self.stored_ammo[ammo] = {
                        'quantity': q,
                        'volume': q*_ammo['volume'],
                        'cost': int(ceil(q*ac)),
                        'magazine_key': k
                    }
                return q
        else:
            self.stored_ammo.pop(ammo, None)
            return 0
    
    def get_capacity(self) -> tuple:
        try:
            if (c:=self.component).cells:
                max = c.cells
                current = sum(c['quantity'] \
                    for c in self.stored_ammo.values())
                return ("Missile Load", ceil(current), max)
            elif self._type == "Magazine":
                max = self.multiple*c.capacity
                current = sum([v['volume'] \
                    for v in self.stored_ammo.values()])
                return ("Stored", ceil(current), max)
            else:
                return None
        except:
            return None
    
    def compatable(self) -> list:
        """List of componenets compatable with this slot."""
        return [n for n,c in components_by_slot[self.slot].items() \
            if _sizecompare(c['size'], self.size)]

    def compounding(self, count: int = 0) -> None:
        comp = ComponentInSocket(self.component.name, self, count)
        self.multiple = comp.multiple
        self._compounding = comp.compounding
        self._power_compounding = comp.power_compounding

    @property
    def component(self) -> ShipComponent:
        return self._component
    
    @component.setter
    def component(self, component_name) -> None:
        if component_name:
            if type(component_name) is str:
                comp: ShipComponent = ShipComponent(component_name)
            else:
                comp: ShipComponent = component_name
            if comp.slot != self.slot:
                raise Exception("Incorrect component type")
            elif not _sizecompare(comp.size, self.size):
                raise Exception("Component too large")
            elif self._component:
                self._reset()
            self._component = comp
            if comp.ammo and "Point Defense" not in comp.type \
                    and comp.xsi_type != "CellLauncherData":
                self.weapon_group_type = comp.ammo
            elif "Jammer" in comp.type:
                self.weapon_group_type = "Jammer"
            elif "Illuminator" in comp.type:
                self.weapon_group_type = "Illuminator"
            elif "Energy" in comp.category:
                self.weapon_group_type = "Energy"
            self._type = self._component.name.split('/')[-1].split(' ')[-1]
            self.compounding()
        else:
            self._reset()
            self._component = None
            
    def _reset(self) -> None:
        self._compounding: int = 1
        self.multiple: int = 1
        self.stored_ammo: dict = {}
        self.weapon_group: str = None
        self.weapon_group_type: str = None
        self._power_compounding: int = 1
        self._type: str = None


class ComponentInSocket(ShipComponent):
        
        def __init__(self, component: str, socket: ShipSocket,
                     socket_count) -> None:
            super().__init__(component)
            self.multiple = 1
            self.compounding = 1
            self.power_compounding = 0
            _cost = self.cost
            socket_type = self.name.split('/')[-1].split(' ')[-1]
            
            def _sizemult() -> int:
                sx, sy, sz = socket.size
                cx, cy, cz = self.size
                return int((sx//cx)*(sy//cy)*(sz//cz))
            f_count = lambda x: socket_count[x] \
                if type(socket_count) is dict else socket_count
            if socket_type ==  "CIC":
                self.multiple = 1
                self.compounding = max(f_count('cic'), 1)
            elif socket_type == "Berthing":
                self.multiple = _sizemult()
                self.compounding = self.multiple if f_count('berthing') else 0
            elif socket_type == "Magazine":
                self.multiple = _sizemult()
                self.compounding = self.multiple if f_count('magazine') else 0
            elif socket_type == "Launcher" \
                    and self.xsi_type == "CellLauncherData" \
                    and "VLS2-16" in self.name:
                self.multiple = 1
                self.compounding = max(3*f_count('cell_launcher'), 1)
            elif "Reactor Booster" in (n:=self.name) or "Control Center" in n:
                if count:=f_count('reactor_booster') > 7:
                    self.power_compounding = 0
                else:
                    self.power_compounding = \
                        [1, 0.92, 0.72, 0.48, 0.27, 0.13, 0.05, 0.02][count]
            else:
                self.multiple = 1
                self.compounding = 1
            self.cost = int(self.compounding*_cost)
            self.crew_provided = int(self.multiple*self.crew_provided) \
                if self.crew_provided else 0
            if self.cells:
                _cap_type = "Missile Load"
                _cap =  self.cells
                self.get_capacity = (_cap_type, _cap)
            elif socket_type == "Magazine":
                _cap_type = "Capacity"
                _cap = self.multiple*self.capacity
                self.get_capacity = (_cap_type, _cap)
            else:
                self.get_capacity = None
            


def _sizecompare(x, y) -> bool:
    return all(a<=b if a and b else False for a,b in zip(sorted(x),sorted(y)))
