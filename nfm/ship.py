import random
import xmltodict

from .shipcomponent import ShipSocket, ShipComponent
from .io import ship_data, ship_component_data, to_xml


class Ship:
    """
    A Nebulous: Fleet Command ship. Contains general information, 
    component slots, equipped components, and weapon groups.
    """

    def load(filepath: str):
        """Imports a ship from .ship template file."""
        with open(filepath) as f:
                x = f.read()
        shipxml = xmltodict.parse(x)
        shipdict = shipxml['Ship']
        return Ship(**{k.lower(): v for k,v in shipdict.items()})

    def __init__(self, hulltype: str, key: str = None, name: str = None, 
                callsign: str = None, number: int = None, 
                symboloption: int = 0, socketmap: dict = None, 
                weapongroups: dict = None, **_) -> None:
        """
        Create a new ship given a hull type and potentially other properties.
        """
        self._key = key
        self._name = name
        self.callsign = callsign
        """
        Optional ship callsign. 
        Displays ingame instead of the ship name.
        """
        self._number = number
        self._symboloption = int(symboloption)
        self.hull_type, hull = Ship._hulltypelookup(hulltype)
        """Name of the hull."""
        self.hull_size: str = hull['hullsize']
        """Hull size category."""
        self._symbols: list = hull['symbols']
        self.base_cost: int = hull['basecost']
        """Point cost of the hull."""
        self.base_crew_complement: int = hull['basecrewcomplement']
        """Crew provided by the hull."""
        self.integrated: list = [ShipComponent(i) 
            for i in hull['integrated']]
        """Integrated components."""
        self.socket_count = {}
        """
        Count of different types of equipped components. 
        Used for determining compounding costs.
        """
        setdefault = not socketmap
        self._mountmap: dict = {
                k: ShipSocket(d, k, setdefault) 
                for k,d in hull['mountkeys'].items()}

        self._compartmentmap: dict = {
                k: ShipSocket(d, k, setdefault) 
                for k,d in hull['compartmentkeys'].items()}

        self._modulemap: dict = {
                k: ShipSocket(d, k, setdefault) 
                for k,d in hull['modulekeys'].items()}

        self._allcomponentsmap = {**self._mountmap, **self._compartmentmap,
                                            **self._modulemap}
        self.mounts = {s.name: s for s in self._mountmap.values()}
        self.compartments = {s.name: s 
                                    for s in self._compartmentmap.values()}
        self.modules = {s.name: s for s in self._modulemap.values()}
        self.all_components = {
            **self.mounts, 
            **self.compartments, 
            **self.modules
        }
        self.ammo = {}
        """
        Both currently stored ammo and ammo required 
        by currently equipped weapons.
        """
        if socketmap and (sm:=socketmap.get('HullSocket')):
            for s in sm:
                self._allcomponentsmap[s['Key']].component \
                                        = s['ComponentName']
                if (d:=s.get('ComponentData')):
                    if (m:=d.get('MissileLoad')) or (m:=d.get('Load')):
                        if type(ammo:= m['MagSaveData']) is not list:
                            ammo = [ammo]
                        for a in ammo:
                            self._allcomponentsmap[s['Key']].store_ammo(
                                            a['MunitionKey'], a['Quantity'])
                        self._recalc_ammo()
                        
        self._compounding()
        self.weapon_groups: dict = {}
        self._fleetlist: list = []
        if weapongroups:
            if type(wepg:=weapongroups.get('WepGroup')) is not list:
                wepg = [wepg]
            for wg in wepg:
                if type(sockets:=wg['MemberKeys']['string']) is not list:
                    sockets = [sockets]
                for socket in sockets:
                    self.add_to_weapon_group(
                        self._mountmap[socket].name, 
                        wg['@Name']
                    )

    def __str__(self) -> str:
        return self.name
    
    def get_component(self, socket: str) -> ShipComponent:
        """Return the equipped component in the given socket."""
        return self.all_components[socket].component

    def set_component(self, socket: str, component=None) -> None:
        """Assigns a component to a socket."""
        if not component:
            self.all_components[socket].component = None
        elif type(component) is ShipComponent:
            self.all_components[socket].component = component
        else:
            self.all_components[socket].component = ShipComponent(component)
        if socket in self.mounts.keys():
            self.remove_from_weapon_groups(socket)
        self._recalc_ammo()
        self._compounding()

    def store_ammo(self, socket: str, ammo: str, quantity: int) -> int:
        """
        Sets the stored amount of ammo of a given type in a given socket.
        """
        q = self.all_components[socket].store_ammo(ammo, quantity)
        self._set_magazine_ammo(socket)
        return q
        
    def power(self) -> int:
        """Total power consumption, in kW, of all equipped components."""
        p = 0
        for c in self._allcomponentsmap.values():
            p += c.power()
        return p

    def power_capacity(self) -> int:
        """Total power provided, in kW."""
        p = 0
        m = 1
        for s in self._allcomponentsmap.values():
            p += s.power_provided()
            m += s.power_mult()
        return int(p*m)

    def crew_required(self) -> int:
        """Total crew required."""
        c = 0
        for s in self._allcomponentsmap.values():
            c += s.crew()
        return c
    
    def crew_complement(self) -> int:
        """Total crew complement."""
        cc = self.base_crew_complement
        for s in self._allcomponentsmap.values():
            cc += s.crew_provided()
        return cc

    def cost(self) -> int:
        """
        Total point cost of the ship hull, equipped modules, 
        and loaded munitions.
        """
        cost = self.base_cost
        for c in self._allcomponentsmap.values():
            cost += c.cost()
        return cost
    
    def new_key(self) -> str:
        """Assigns a random key."""
        keystr = random.choices('0123456789abcdef', k=32)
        for i in [8, 13, 18, 23]:
            keystr.insert(i,'-')
        self._key = ''.join(keystr)
        return self._key

    def new_number(self, numrange: tuple=(1,2000)) -> int:
        """Assigns a random number. Default range is between 1 and 2000."""
        self._number = random.randint(*numrange)
        return self._number

    def add_to_weapon_group(self, socket: str, group: str) -> None:
        """
        Adds the socket to a weapon group. For an existing group, 
        verifies that the weapon is compatable with the group. 
        Creates the group if one with that name does not exist.
        """
        if (s:=self.mounts[str(socket)]).component:  
            if (g:=self.weapon_groups.get(group)):
                if g['type'] == s.weapon_group_type:
                    self.remove_from_weapon_groups(s.name)
                    g['mounts'].append(s)
                    s.weapon_group = group
            elif s.weapon_group_type:
                self.remove_from_weapon_groups(s.name)
                self.weapon_groups[group] = {
                    'type': s.weapon_group_type, 'mounts':[s]
                }
                s.weapon_group = group
            else:
                raise Exception("Cannot be added to a weapon group.")
    
    def remove_from_weapon_groups(self, socket: str) -> None:
        """Removes the weapon from all weapon groups."""
        if (group:=self.mounts[str(socket)].weapon_group):
            try:
                if (s:=self.mounts[str(socket)]) \
                    in (g:=self.weapon_groups[group]['mounts']):
                    g.remove(s)
                    if not len(g):
                        del self.weapon_groups[group]
            finally:
                s.weapon_group = None
    
    def get_weapon_groups(self) -> list:
        return [k for k in self.weapon_groups]
    
    def get_compatable_weapon_groups(self, socket: str) -> list:
        return [k for k,v in self.weapon_groups.items()
            if v['type'] == self.mounts[str(socket)].weapon_group_type]

    def validate(self):
        self._recalc_ammo()
        errors = []
        if self.power() > self.power_capacity():
            errors.append("Insufficient Power")
        if self.crew_required() > self.crew_complement():
            errors.append("Insufficient Crew")
        empty_launchers = []
        missing_groups = []
        for mount in self.mounts.values():
            if (c:=mount.get_capacity()) and c[0] == "Missile Load" \
                and not c[1]:
                empty_launchers.append(mount.name)
            elif mount.weapon_group_type and not mount.weapon_group:
                missing_groups.append(mount.name)
        if empty_launchers:
            errors.append({"Empty Launcher": empty_launchers})
        no_weapons = []
        no_ammo = []
        for ammo,details in self.ammo.items():
            if details.get('stored') and not details.get('weapons'):
                no_weapons.append(ammo)
            elif not details.get('stored') and (w:=details.get('weapons')):
                no_ammo.extend(w)
        if no_weapons:
            errors.append({"Unused Ammo": no_weapons})
        if no_ammo:
            errors.append({"Missing Ammo": no_ammo})
        if missing_groups:
            errors.append({"Missing Group Assignment": missing_groups})
        return errors
        # Check weapon groups
        
    
    def report(self, full: bool = False, out: bool=True):
        """
        Ship information, including all equipped components, 
        in an easy-to-read format.
        """
        report = [
            f"Name: {self.name}",
            f"{self.symbol}-{self.number}",
            f"Class: {self.hull_type}"
        ]
        if self.callsign:
            report.append(f"Callsign: {self.callsign}")
        report.extend([
            f"Cost: {self.cost()}",
            f"Crew: {self.crew_required()}/{self.crew_complement()}",
            f"Power: {self.power()}/{self.power_capacity()} kW"
        ])
        if i:=self.integrated:
            report.append(
                f"Integrated Components: {', '.join([c.name for c in i])}"
            )
        
        for s, c in self.all_components.items():
            if (comp:=c.component):
                if full:
                    cost = c.cost()
                    crew = c.crew()
                    power = c.power()
                    report.append(
                        f"{s}: {comp.name} [Cost: {cost},"
                        f"Crew: {crew}, Power: {power}]"
                    )
                else:
                    report.append(f"{s}: {comp.name}")
                if (a:=c.stored_ammo):
                    report.append("    Stored Ammo:")
                    for k,v in a.items():
                        ac =  f" [Cost: {v['cost']}]" if full else ""
                        report.append(f"       {k}: {v['quantity']}{ac}")
            else:
                report.append(f"{s}: Empty")
        report = '\n'.join(report)
        if out:
            print(report)
        else:
            return report

    def save(self, filepath: str=None, 
                tofleet: bool = False):
        """
        Formats ship data for export, either as part of a fleet, 
        or for an individual ship template.
        """
        outdict = {
            'SaveID': None,
            'Key': self.key,
            'Name': self._name,
            'Cost': self.cost()
        }
        if self.callsign:
            outdict['Callsign'] = self.callsign
        outdict['Number'] = self.number
        outdict['SymbolOption'] = self._symboloption
        outdict['HullType'] =  self.hull_type
        socketmap =  []
        for k,v in self._allcomponentsmap.items():
            if c := v.component:
                d = {'Key': k, 'ComponentName': c.name}
                if x:=c.xsi_type:
                    d['ComponentData'] = {'_xsi-type': x}
                if ammo:=v.stored_ammo:
                    mag = [
                        {
                            'MagazineKey': q['magazine_key'], 
                            'MunitionKey': a, 
                            'Quantity': q['quantity']
                        } for a,q in ammo.items()
                    ]
                    if x == "CellLauncherData":
                        d['ComponentData']['MissileLoad'] = mag
                    elif x == "BulkMagazineData":
                        d['ComponentData']['Load'] = mag
                socketmap.append(d)
        outdict['SocketMap'] = socketmap
        outdict['WeaponGroups'] = [
            {
                'Name': k, 
                'MemberKeys': [
                    m.key for m in v['mounts']
                ]
            } for k,v in self.weapon_groups.items()
        ]
        if tofleet:
            return outdict
        else:
            to_xml(outdict, filepath, fleet=False)
    
    def register_fleet(self, fleet) -> None:
        """
        Ships are aware of their owning fleets(s), 
        so that when their name changes they can update the 
        fleet registries.
        """
        if fleet not in self._fleetlist:
            self._fleetlist.append(fleet)

    def deregister_fleet(self, fleet) -> None:
        """Removes a fleet from the list of owning fleets."""
        self._fleetlist.remove(fleet)

    def _recalc_ammo(self) -> None:
        self.ammo = {}
        for name,socket in self.all_components.items():
            if (comp:=socket.component) and not comp.cells:
                if socket.stored_ammo:
                    self._set_magazine_ammo(name)
                elif comp.ammo:
                    self._set_weapon_ammo(name)
                
    def _set_magazine_ammo(self, socket: str) -> None:
        if (
            (comp:=self.all_components[socket].component) 
            and (sa:=self.all_components[socket].stored_ammo) 
            and not comp.cells
        ):
            for t,q in sa.items():
                if (a:=ship_component_data.get(t)['type']) in self.ammo:
                    if (s:=self.ammo[a].get('stored')):
                        if t in s.keys():
                            self.ammo[a]['stored'][t][socket] = q['quantity']
                        else:
                            self.ammo[a]['stored'][t] = {socket: 
                                                            q['quantity']}
                    else:
                        self.ammo[a]['stored'] = {t: {socket: q['quantity']}}
                    if not self.ammo[a].get('weapons'):
                        self.ammo[a]['weapons'] = []
                else:
                    self.ammo[a] = {
                        'stored': {t: {socket: q['quantity']}},
                        'weapons': []
                    }
                    
        
    def _set_weapon_ammo(self, socket: str) -> None:
        if (comp:=self.all_components[socket].component) and not comp.cells:
            if (ammo:=comp.ammo):
                if ammo in self.ammo:
                    if (w:=self.ammo[ammo].get('weapons')):
                        if socket in w:
                            w.sort()
                        else:
                            self.ammo[ammo]['weapons'].append(socket)
                    else:
                        self.ammo[ammo]['weapons'] = [socket]
                    if not self.ammo[ammo].get('stored'):
                        self.ammo[ammo]['stored'] = {}
                else:
                    self.ammo[ammo] = {
                        'stored': {},
                        'weapons': [socket]
                    }

    def _compounding(self) -> None:
        cic = ber = mag = l16 = pwr = 0
        for s in sorted([v for v in self._allcomponentsmap.values()if v.component]):
            try:
                if (n:= s.component.name).endswith("CIC"):
                    s.compounding(cic)
                    cic += 1
                elif n.endswith("Berthing"):
                    s.compounding(ber)
                    ber += 1
                elif n.endswith("Magazine"):
                    s.compounding(mag)
                    mag += 1
                elif "VLS2-16" in n:
                    s.compounding(l16)
                    l16 += 1
            except:
                continue
        for s in self._allcomponentsmap.values():
            try:
                if ("Reactor Booster" in (n:=s.component.name) 
                    or "Control Center" in n):
                    s.compounding(pwr)
                    pwr += 1
            except:
                continue
        self.socket_count = {
            'cic': cic,
            'berthing': ber,
            'magazine': mag,
            'cell_launcher': l16,
            'reactor_booster': pwr
        }

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, name: str) -> None:
        oldname = self._name
        self._name = name
        # fl = self._fleetlist.copy()
        for fleet in self._fleetlist.copy():
            fleet.remove_ship(oldname)
            fleet.add_ship(self)
        
    @property
    def key(self) -> str:
        if not self._key:
            self.new_key()
        return self._key
    
    @key.setter
    def key(self, key: str) -> None:
        self._key = key

    @property
    def number(self) -> int:
        if not self._number:
            self.new_number()
        return self._number
    
    @number.setter
    def number(self, number: int) -> None:
        self._number = int(number)

    @property
    def symbol(self) -> str:
        return self._symbols[self._symboloption]
    
    @symbol.setter
    def symbol(self, sym: str) -> None:
        try:
            self._symboloption = self._symbols.index(sym.upper())
        except:
            pass

    @staticmethod
    def lookup(hull_type: str) -> None:
        """Returns a report on the stats and sockets of a given hull type."""
        hulltype, hull = Ship._hulltypelookup(hull_type)
        report =[
            f"Class: {hulltype}",
            f"Base Point Cost: {hull['basecost']}",
            f"Base Crew Complement: {hull['basecrewcomplement']}"
        ]
        if (i:=hull['integrated']):
            report.append(
                f"Integrated Components: {', '.join([c for c in i])}"
            )
        m = {
            'Spinal': 0,
            'Class 5': 0,
            'Class 4': 0,
            'Class 3': 0,
            'Class 2': 0,
            'Class 1': 0,   
        }
        for mount in hull['mountkeys'].values():
            if (s:=sorted(mount['size'])) == sorted((4, 12, 4)):
                m['Spinal'] += 1
            elif s == sorted((8, 7, 8)):
                m['Class 5'] += 1
            elif s == sorted((6, 4, 6)):
                m['Class 4'] += 1
            elif s == sorted((3, 4, 5)):
                m['Class 3'] += 1
            elif s == sorted((3, 4, 3)):
                m['Class 2'] += 1
            elif s == sorted((2, 2, 2)):
                m['Class 1'] += 1
        report.append("Weapon Mounts:")
        report.extend(
                [f"  {k}: {v}" for k,v in m.items() if v]
        )
        complist = []
        for comp in hull['compartmentkeys'].values():
            complist.append(comp['size'])
        f_sort = lambda x,y,z: [(s:=sorted([x,z]))[0], y, s[1]]
        complist = [f_sort(*c) for c in complist]
        complist.sort(key=sorted, reverse=True)
        c2 = [sorted(x) for x in complist]
        report.append("Compartments:")
        for comp in complist:
            if (count:=c2.count(s:=sorted(comp))):
                for _ in range(count):
                    c2.remove(s)
                x,y,z = comp
                report.append(f"  {x}x{y}x{z}: {count}")
        modulelist = []
        for module in hull['modulekeys'].values():
            modulelist.append(module['size'])
        modulelist = [f_sort(*c) for c in modulelist]
        modulelist.sort(key=sorted, reverse=True)
        m2 = [sorted(x) for x in modulelist]
        report.append("Modules:")
        for module in modulelist:
            if (count:=m2.count(s:=sorted(module))):
                for _ in range(count):
                    m2.remove(s)
                x,y,z = module
                report.append(f"  {x}x{y}x{z}: {count}")
        report = '\n'.join(report)
        print(report)
    
    @staticmethod
    def _hulltypelookup(hull_type: str) -> tuple:
        if hull_type in ship_data.keys():
            _hulltype = hull_type
        elif (h:=f"Stock/{hull_type}") in ship_data.keys():
            _hulltype = h
        elif (h:=''.join(hull_type.lower().split())) == 'corvette':
            _hulltype = "Stock/Sprinter Corvette"
        elif hull_type.lower() == 'frigate':
            _hulltype = "Stock/Raines Frigate"
        elif h == 'destroyer':
            _hulltype = "Stock/Keystone Destroyer"
        elif h == 'lightcruiser':
            _hulltype = "Stock/Vauxhall Light Cruiser"
        elif h == 'heavycruiser':
            _hulltype = "Stock/Axford Heavy Cruiser"
        elif h == 'battleship':
            _hulltype = "Stock/Solomon Battleship"
        else:
            raise Exception("Unknown hull type")
        return _hulltype, ship_data[_hulltype]