"""
Nebulous Fleet Manager.

This module creates and edits fleets for the game NEBULOUS: Fleet Command.

The Fleet class contains the list of ships, as well as fleet data.

The Ship class contains ship details, including a list of sockets and their 
assigned components.

The ShipComponent class contains details for a specific component type.
"""
from .fleet import Fleet
from .ship import Ship
from .shipcomponent import ShipSocket, ShipComponent, ComponentInSocket
from .io import ship_classes, ship_component_data, ammo_by_type

__all__ = (
    'Fleet',
    'Ship',
    'ShipComponent'
)
