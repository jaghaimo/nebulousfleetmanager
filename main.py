import os
# from kivy.config import Config
from kivy.core.window import Window
from kivy.utils import platform
from kivymd.app import MDApp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.filemanager import MDFileManager

from nfm import Fleet, Ship

import nfmapp
from nfmapp import commonelements as ce


class SaveDialog(MDBoxLayout):
    pass


class ConfirmOverwriteDialog(MDBoxLayout):
    pass


class ConfirmCloseDialog(MDBoxLayout):
    pass


class AppFileManager(MDFileManager):

    def on_touch_move(self, touch):
        if touch.x - touch.ox > ce.min_swipe_length:
            ce.app.exit_manager()


class MainApp(MDApp):

    save_dialog: MDDialog = None
    confirm_overwrite_dialog: MDDialog = None
    confirm_fleet_change_dialog: MDDialog = None
    ship_delete_dialog: MDDialog = None
    ammo_dialog: MDDialog = None
    weapon_group_dialog: MDDialog = None
    confirm_close_dialog: MDDialog = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Window.bind(on_request_close=self.confirm_close)
        # Config.set('kivy', 'keyboard_mode', 'multi')
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'DeepOrange'
        self.theme_cls.material_style = 'M3'
        self.manager_open = False
        self.file_manager = AppFileManager(
            exit_manager=self.exit_manager,
            select_path=self.select_path,
        )
        self.close_app: bool = False
        self._platform_specific()

    def run(self):
        nfmapp.load()
        super().run()

    def open_screen(self, screen: str, direction: str='left'):
        try:
            if not screen.endswith('_screen'):
                screen = f'{screen}_screen'
            if screen == 'fleet_screen':
                self.root.ids.sm.transition.direction = 'right'
            else:
                self.root.ids.sm.transition.direction = direction
            self.root.ids.sm.current = screen
        except:
            pass

    def file_manager_open(self, fmtype: str='fleet', fmsave: bool=False):
        if not fmsave or ce.fleet and fmtype == 'fleet' or ce.ship and fmtype == 'ship':
            self.file_manager.ext = [f'.{fmtype}']
            self.file_manager.selector = 'any' if fmsave else 'file'
            self.file_manager.show(path=self.default_path)
            # self.file_manager.show_disks()
            self.manager_open = True

    def select_path(self, path: str):
        '''It will be called when you click on the file name
        or the catalog selection button.

        :type path: str;
        :param path: path to the selected directory or file;
        '''
        if self.file_manager.selector == 'file':
            self.exit_manager()
            if path.endswith('.fleet'):
                ce.fleet = Fleet.load(path)
                try:
                    self.root.ids.fleet_screen.load_fleet()
                except:
                    pass
                else:
                    self.open_screen('fleet_screen')
            elif path.endswith('.ship'):
                if not ce.fleet:
                    ce.fleet = Fleet("New Fleet")
                ce.ship = Ship.load(path)
                ce.fleet.add_ship(ce.ship)
                self.root.ids.fleet_screen.ids.fleet_tabs.ids\
                    .fleet_ships_tab._add_ship(ce.ship)
        elif self.file_manager.selector == 'any':
            self.save_path = path
            if path.endswith('.fleet'):
                self.show_confirm_overwrite_dialog(file_type='fleet')
            elif path.endswith('.ship'):
                self.show_confirm_overwrite_dialog(file_type='ship')
            else:
                self.show_save_dialog()

    def show_save_dialog(self):
        if not self.save_dialog:
            self.save_dialog = MDDialog(
                title="File Name:",
                type="custom",
                content_cls=SaveDialog(),
                buttons=[
                    MDFlatButton(
                        text="CANCEL", 
                        on_press=lambda x: self.save_dialog.dismiss()
                    ),
                    MDFlatButton(
                        text="SAVE", 
                        on_press=lambda x: \
                            self.save_file(path_type='dir')
                    )
                ]
            )
        else:
            self.save_dialog.content_cls.ids.file_name.text = ''
        self.save_dialog.open()

    def show_confirm_overwrite_dialog(self, file_type: str='fleet'):
        title = f"Overwrite {file_type.capitalize()}?"
        if not self.confirm_overwrite_dialog:
            self.confirm_overwrite_dialog = MDDialog(
                type='custom',
                content_cls=ConfirmOverwriteDialog(),
                buttons=[
                    MDFlatButton(
                        text="CANCEL", 
                        on_press=lambda x: \
                            self.confirm_overwrite_dialog.dismiss()
                    ),
                    MDFlatButton(
                        text="SAVE", 
                        on_press=lambda x: \
                            self.save_file(path_type='file')
                    )
                ]
            )
        self.confirm_overwrite_dialog.title = title
        self.confirm_overwrite_dialog.open()

    def save_file(self, *args, path_type: str, **kwargs):
        if ce.fleet and self.file_manager.ext == ['.fleet']:
            if path_type == 'dir':
                filename: str \
                    = self.save_dialog.content_cls.ids.file_name.text
                if not filename.endswith('.fleet'):
                    filename += '.fleet'
                self.save_path = f'{self.save_path}/{filename}'
                self.save_dialog.dismiss()
                if os.path.exists(self.save_path):
                    self.show_confirm_overwrite_dialog(file_type='fleet')
                    return
                else:
                    ce.fleet.save(self.save_path)
            elif path_type == 'file':
                self.confirm_overwrite_dialog.dismiss()
                ce.fleet.save(self.save_path)
        elif ce.ship and self.file_manager.ext == ['.ship']:
            if path_type == 'dir':
                filename: str \
                    = self.save_dialog.content_cls.ids.file_name.text
                if not filename.endswith('.ship'):
                    filename += '.ship'
                self.save_path = f'{self.save_path}/{filename}'
                self.save_dialog.dismiss()
                if os.path.exists(self.save_path):
                    self.show_confirm_overwrite_dialog(file_type='ship')
                    return
                else:
                    ce.ship.save(self.save_path)
            elif path_type == 'file':
                self.confirm_overwrite_dialog.dismiss()
                ce.ship.save(self.save_path)
        self.exit_manager()

    def exit_manager(self, *args):
        self.manager_open = False
        self.file_manager.close()
        if self.close_app:
            self.stop()

    def confirm_close(self, *args, **kwargs) -> bool:
        if self.close_app:
            return False
        elif self.manager_open:
            self.exit_manager()
        elif (sm:=self.root.ids.sm).current == 'fleet_screen':
            if (ft:=sm.current_screen.ids.fleet_tabs).carousel.current_slide \
                == ft.ids.fleet_ships_tab:
                ft.ids.fleet_ships_tab.manage_ship_menu.dismiss()
                ft.ids.fleet_ships_tab.new_ship_menu.dismiss()
                ft.carousel.load_previous()
            elif ce.fleet:
                self.show_confirm_close_dialog()
            else:
                return False
        else:
            sm.current_screen.go_back()
        return True

    def show_confirm_close_dialog(self):
        if not self.confirm_close_dialog:
            self.confirm_close_dialog = MDDialog(
                title="Save Before Close?",
                type='custom',
                content_cls=ConfirmCloseDialog(),
                buttons=[
                    MDFlatButton(
                        text="CANCEL", 
                        on_press=self._cancel_close_button
                    ),
                    MDFlatButton(
                        text="CLOSE", 
                        on_press=self._confirm_close_button
                    ),
                    MDFlatButton(
                        text="SAVE", 
                        on_press=self._save_before_close_button
                    )
                ]
            )
        self.confirm_close_dialog.open()

    def _cancel_close_button(self, *args, **kwargs):
        self.close_app = False
        self.confirm_close_dialog.dismiss()

    def _confirm_close_button(self, *args, **kwargs):
        self.close_app = True
        self.confirm_close_dialog.dismiss()
        self.stop()

    def _save_before_close_button(self, *args, **kwargs):
        self.close_app = True
        self.confirm_close_dialog.dismiss()
        self.file_manager_open(fmsave=True)
    
    def _platform_specific(self):
        if platform == 'android':
            from android.permissions import Permission, request_permissions # type: ignore
            from jnius import autoclass # type: ignore
            request_permissions([
                Permission.READ_EXTERNAL_STORAGE, 
                Permission.WRITE_EXTERNAL_STORAGE
            ])
            self.default_path = '/sdcard/Documents/NebulousFleetManager'
            os.makedirs(self.default_path, exist_ok=True)
            PythonActivity = autoclass('org.kivy.android.PythonActivity')
            ActivityInfo = autoclass('android.content.pm.ActivityInfo')
            activity = PythonActivity.mActivity
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER)
        else:
            self.default_path = self.file_manager.current_path
        self.save_path = self.default_path


if __name__ == '__main__':
    ce.app = MainApp()
    ce.app.run()
