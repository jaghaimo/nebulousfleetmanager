from kivy.clock import Clock
from kivy.metrics import dp
from kivy.uix.scrollview import ScrollView
from kivymd.uix.behaviors import TouchBehavior
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDFlatButton, MDFloatingActionButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.label import MDLabel
from kivymd.uix.list import (MDList, OneLineAvatarListItem, ThreeLineListItem,
                             TwoLineListItem)
from kivymd.uix.menu import MDDropdownMenu
from nfm import ComponentInSocket as cis
from nfm import ship_component_data

from . import commonelements as ce


class ComponentSelectScreen(ce.NFMScreen):

    prev_screen = 'ship_screen'

    def on_pre_enter(self, *args):       
        complist = ce.socket.compatable()
        compcatagories = []
        if ce.socket.component:
            self.ids.components.add_widget(
                RemoveComponentListItem()
            )
        for c in complist:
            comp = cis(c,ce.socket,ce.ship.socket_count)
            if (cat:=comp.category) not in compcatagories:
                self.ids.components.add_widget(
                    ce.ListSeparator(text=cat)
                )
                compcatagories.append(cat)
            text = c
            secondary_text=f"{comp.type} ({comp.cost} pts)"
            if power:=comp.power_provided:
                secondary_text += f": Provides {power} kW"
            if crew:=comp.crew_provided:
                secondary_text += f": Supports {crew} Crew"
            if (cap:= comp.get_capacity):
                secondary_text += f": {cap[0]}: {cap[1]}"
            x,y,z = comp.size
            mult = f" (x{m})" if (m:=comp.multiple)>1 else ""
            tertiary_text = f"Dimensions: {x}x{y}x{z}{mult}"
            if power:=comp.power:
                tertiary_text += f" / Power: {power} kW"
            if crew:=comp.crew:
                tertiary_text += f" / Crew: {crew}"
            self.ids.components.add_widget(
                ComponentListItem(
                    text=text,
                    secondary_text=secondary_text,
                    tertiary_text=tertiary_text
                )
            )
        super().on_pre_enter(*args)

    def on_leave(self, *args):
        ce.comp = None
        self.ids.components.clear_widgets()
        super().on_leave(*args)


class ComponentListItem(TouchBehavior,ThreeLineListItem):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.lt = False

    def on_release(self):
        if not self.lt:
            self.set_component()
            ThreeLineListItem().on_release()
        else:
            self.lt = False

    def on_long_touch(self, touch, *args):
        self.lt = True
        super().on_long_touch(touch, *args)

    def set_component(self):
        ce.ship.set_component(ce.socket.name, self.text.split(' - ')[0])
        ce.socket_widget.text = (c.name 
                                 if (c:=ce.socket.component) else "Empty")
        ce.app.open_screen('ship_screen', direction='right')


class RemoveComponentListItem(OneLineAvatarListItem):

    def remove_component(self):
        ce.ship.set_component(ce.socket.name)
        ce.socket_widget.text = "Empty"
        ce.app.open_screen('ship_screen', direction='right')


class AmmoDialog(MDBoxLayout):
    pass


class WeaponGroupDialog(MDBoxLayout):
    pass


class AmmoButton(MDFloatingActionButton):
    pass


class ComponentDetailsScreen(ce.NFMScreen):

    prev_screen = 'ship_screen'

    def on_pre_enter(self, *args):
        def _addlabel(text, font_style):
            label = ce.NFMLabel(
                    text=text,
                    font_style=font_style
            )
            self.ids.details.add_widget(
                label
            )
            return label
        self.ammo_button = None
        _addlabel(ce.comp.name, 'H4')
        self.cost_label = _addlabel(
            f"{ce.comp.type} ({ce.socket.cost()} pts)",
            'H5'
        )
        x,y,z = ce.comp.size
        dim = f"Dimensions: {x}x{y}x{z}"
        if (m:=ce.socket.multiple) > 1:
            dim += f" (x{m})"
        details = [dim]
        if (c:=ce.socket.crew()):
            details.append(f"Crew Required: {c}")
        if (c:=ce.socket.crew_provided()):
            details.append(f"Crew Provided: {c}")
        if (p:=ce.socket.power()):
            details.append(f"Power Required: {p} kW")
        if (p:=ce.socket.power_provided()):
            details.append(f"Power Provided: {p} kW")
        for d in details:
            _addlabel(d, 'Body1')
        if (cap:=ce.socket.get_capacity()):
            sv = ScrollView()
            self.ammo_list = MDList()
            self.ammo_label = ce.LabelWithLine(
                text=f"{cap[0]}: {cap[1]}/{cap[2]}"
            )
            self.ammo_label.font_style = 'H6'
            self.ids.details.add_widget(self.ammo_label)
            self.ids.details.add_widget(sv)
            sv.add_widget(self.ammo_list)
            if cap[0] == "Missile Load":
                ammo = {
                    a: EquippedMissileListItem(
                        text=f"{a}: {q['quantity']}",
                        secondary_text="Total Cost: " \
                            f"{ce.socket.stored_ammo[a]['cost']}",
                        screen = self
                    ) 
                    for a,q in ce.socket.stored_ammo.items()
                }
            elif cap[0] == "Stored":
                ammo = {
                    a: EquippedAmmoListItem(
                        text=f"{a}: {q['quantity']}",
                        secondary_text=f"{ship_component_data[a]['type']} " \
                            "(Total Cost: "\
                            f"{ce.socket.stored_ammo[a]['cost']})",
                        tertiary_text = "Total Volume: " \
                            f"{ce.socket.stored_ammo[a]['volume']}",
                        screen = self
                    ) 
                    for a,q in ce.socket.stored_ammo.items()
                }
            for a in ammo.values():
                self.ammo_list.add_widget(a)
            Clock.schedule_once(self._add_ammo_button, 0)
        elif ce.socket.weapon_group_type:
            group = wg if (wg:=ce.socket.weapon_group) else "<Unassigned>"
            # replace with MDDropDownItem
            self.weapon_group_label = ce.NFMLabel(
                text=f"[ref=wg]Weapon Group: {group}[/ref]",
                markup=True
            )
            self.weapon_group_label.font_style = 'H6'
            self.ids.details.add_widget(self.weapon_group_label)
            menuitems = [
                {
                    'text': "<Unassigned>",
                    'viewclass': 'OneLineListItem',
                    'on_release': self.set_group
                }
            ]
            if (groups:=ce.ship.get_compatable_weapon_groups(ce.socket)):
                menuitems.extend(
                    {
                        'text': g,
                        'viewclass': 'OneLineListItem',
                        'on_release': lambda x=g: self.set_group(x)
                    } for g in groups
                )
            menuitems.append(
                {
                    'text': "<New Group>",
                    'viewclass': 'OneLineListItem',
                    'on_release': self.open_new_group_dialog
                }
            ) 
            self.weapon_group_menu = MDDropdownMenu(
                caller=self.weapon_group_label,
                items=menuitems,
                max_height=dp(48*(len(menuitems))),
                width_mult=4,
                position="bottom"
            )
            self.weapon_group_label.bind(
                on_ref_press=lambda *x: self.weapon_group_menu.open()
            )
        self.ids.details.add_widget(
            MDLabel(size_hint=(1,1))
        )
        super().on_pre_enter(*args)

    def _add_ammo_button(self, *args):
        self.ammo_button = AmmoButton()
        self.ids.main_layout.add_widget(self.ammo_button)
        self.ammo_button.bind(on_release=lambda x: \
            ce.app.open_screen('ammo_select_screen'))
        self.ammo_button.right = self.ammo_button.parent.right - 50

    def on_leave(self, *args):
        self.ids.details.clear_widgets()
        if self.ammo_button:
            self.ids.main_layout.remove_widget(self.ammo_button)
        self.ammo_button = None
        super().on_leave(*args)

    def set_group(self, group: str=None) -> None:
        if group:
            ce.ship.add_to_weapon_group(ce.socket, group)
        else:
            ce.ship.remove_from_weapon_groups(ce.socket)
            group ="<Unassigned>"
        self.weapon_group_label.text = \
            f"[ref=wg]Weapon Group: {group}[/ref]"
        self.weapon_group_menu.dismiss()

    def new_group(self):
        group = ce.app.weapon_group_dialog.content_cls.ids.weapon_group.text
        ce.ship.add_to_weapon_group(ce.socket, group)
        self.weapon_group_label.text = f"[ref=wg]Weapon Group: {group}[/ref]"
        ce.app.weapon_group_dialog.dismiss()

    def open_new_group_dialog(self):
        self.weapon_group_menu.dismiss()
        screen = self
        if not ce.app.weapon_group_dialog:
            ce.app.weapon_group_dialog = MDDialog(
                title="Weapon Group:",
                type="custom",
                content_cls=WeaponGroupDialog(),
                buttons=[
                    MDFlatButton(
                        text="CANCEL",
                        on_press=lambda x: \
                            ce.app.weapon_group_dialog.dismiss()
                    ),
                    MDFlatButton(
                        text="ENTER",
                        on_press=lambda x: screen.new_group()
                    )
                ]
            )
        else:
            ce.app.weapon_group_dialog.content_cls.ids.weapon_group.text = ''
        ce.app.weapon_group_dialog.open()


class EquippedMissileListItem(TouchBehavior, TwoLineListItem):

    __screen: ComponentDetailsScreen = None

    def __init__(self, /, screen: ComponentDetailsScreen, **kwargs):
        super().__init__(**kwargs)
        self.lt = False
        if not self.__screen:
            self.__screen = screen

    def on_release(self):
        if not self.lt:
            self.open_ammo_dialog()
        else:
            self.lt = False

    def on_long_touch(self, touch, *args):
        self.lt = True
        super().on_long_touch(touch, *args)

    def open_ammo_dialog(self):
        if not ce.app.ammo_dialog:
            ce.app.ammo_dialog = MDDialog(
                title="Ammo Amount:",
                type="custom",
                content_cls=AmmoDialog(),
                buttons=[
                    MDFlatButton(
                        text="CANCEL",
                        on_press=lambda x: ce.app.ammo_dialog.dismiss()
                    ),
                    MDFlatButton(
                        text="REMOVE",
                        on_press=lambda x: \
                            ce.app.ammo_dialog.ammo_widget.update_ammo(
                                remove=True
                            )
                    ),
                    MDFlatButton(
                        text="ENTER",
                        on_press=lambda x: \
                            ce.app.ammo_dialog.ammo_widget.update_ammo()
                    )
                ]
            )
        ce.app.ammo_dialog.content_cls.ids.ammo_count.text = \
            self.text.split(': ')[1]
        ce.app.ammo_dialog.ammo_widget = self
        ce.app.ammo_dialog.open()

    def update_ammo(self, remove: bool = False):
        ammo = self.text.split(':')[0]
        quantity = ce.app.ammo_dialog.content_cls.ids.ammo_count.text \
            if not remove else 0
        q = ce.ship.store_ammo(ce.socket.name, ammo, quantity)
        self.__screen.cost_label.text = \
            f"{ce.comp.type} ({ce.socket.cost()} pts)"
        cap_type, cap_min, cap_max = ce.socket.get_capacity()
        self.__screen.ammo_label.text=f"{cap_type}: {cap_min}/{cap_max}"
        ce.app.ammo_dialog.dismiss()
        if ammo and not remove:
            self.update_text(ammo,q)
        else:
            self.__screen.ammo_list.remove_widget(self)

    def update_text(self, ammo, quantity):
        self.text = f"{ammo}: {quantity}"
        self.secondary_text = \
            f"Total Cost: {ce.socket.stored_ammo[ammo]['cost']}"


class EquippedAmmoListItem(ThreeLineListItem, EquippedMissileListItem):

    def update_text(self, ammo, quantity):
        super().update_text(ammo, quantity)
        self.secondary_text = f"{ship_component_data[ammo]['type']} " \
            f"({self.secondary_text})"
        self.tertiary_text = \
            f"Total Volume: {ce.socket.stored_ammo[ammo]['volume']}"
