from kivymd.uix.behaviors import TouchBehavior
from kivymd.uix.list import OneLineListItem, ThreeLineListItem
from kivymd.uix.screen import MDScreen

from nfm import ammo_by_type

from . import commonelements as ce


class AmmoSelectScreen(ce.NFMScreen):

    prev_screen = 'component_details_screen'

    def on_pre_enter(self, *args):
        ammolist = []
        if (captype:= ce.socket.get_capacity()):
            for ammo_type,ammo in ammo_by_type.items():
                if (
                    captype[0] == 'Missile Load' 
                    and ce.socket.component.ammo == ammo_type
                ):
                    ammolist.extend(ammo)
                    for k,v in ammo.items():
                        self.ids.ammo.add_widget(
                            CellAmmoListItem(
                                text=f"{k} ({v['cost']} pts.)"
                            )
                        )
                elif (
                    captype[0] == 'Stored' 
                    and (a:=ce.ship.ammo.get(ammo_type))
                ):
                    weapons = [c for m in a['weapons'] if 
                               not (c:=ce.ship.get_component(m)).cells]
                    if weapons:
                        ammolist.extend(ammo)
                        for k,v in ammo.items():
                            c = str(v['cost']).split('/')
                            cost = (f"{c[0]} pts. per {c[1]} units" 
                                if len(c)>1 else f"{c[0]} pts.")
                            self.ids.ammo.add_widget(
                                MagAmmoListItem(
                                    text=k,
                                    secondary_text=f"{v['type']} ({cost})",
                                    tertiary_text=
                                        f"Volume per unit: {v['volume']}"
                                )
                            )

    def on_leave(self, *args):
        self.ids.ammo.clear_widgets()
        super().on_leave(*args)


class CellAmmoListItem(TouchBehavior,OneLineListItem):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.lt = False

    def on_release(self):
        if not self.lt:
            self.add_ammo()
        else:
            self.lt = False

    def on_long_touch(self, touch, *args):
        self.lt = True
        super().on_long_touch(touch, *args)

    def add_ammo(self):
        ammo = self.text.split(" (")[0]
        q = a[ammo]['quantity'] if ammo in (a:=ce.socket.stored_ammo) else 0
        if ce.ship.store_ammo(ce.socket.name, ammo, q+1) == q:
            # Show "Insufficient Space" messege
            pass
        ce.app.open_screen('component_details_screen', direction='right')


class MagAmmoListItem(ThreeLineListItem,CellAmmoListItem):
    pass


class AmmoDetailsScreen(MDScreen):
    pass
