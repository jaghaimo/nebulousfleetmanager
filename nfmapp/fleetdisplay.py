from kivy.metrics import dp
from kivymd.uix.behaviors import TouchBehavior
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.floatlayout import MDFloatLayout
from kivymd.uix.list import ThreeLineListItem
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.stacklayout import MDStackLayout
from kivymd.uix.tab import MDTabs, MDTabsBase

from nfm import Fleet, Ship, ship_classes

from . import commonelements as ce


class ConfirmFleetChangeDialog(MDBoxLayout):
    pass


class FleetScreen(ce.NFMScreen):

    next_screen = 'ship_screen'

    def on_enter(self, *args):
        ce.socket_widget = None
        super().on_enter(*args)

    def load_fleet(self):
        f: FleetTabs = self.ids.fleet_tabs
        f.ids.fleet_name.text = ce.fleet.name
        f.ids.fleet_description.text = d if (d:=ce.fleet.description) else ''
        f.ids.fleet_ships_tab.ids.ship_list.clear_widgets()
        if not self.new_fleet:  
            for s in ce.fleet.ships.values():
                f.ids.fleet_ships_tab.ids.ship_list.add_widget(
                    ShipListItem(
                        text=s.name,
                        secondary_text=s.hull_type,
                        tertiary_text=f"Cost: {s.cost()}",
                        tab=f.ids.fleet_ships_tab
                    )
            )
        f.update_cost(self.new_fleet)

    def on_touch_move(self, touch):
        if (self.ids.fleet_tabs.carousel.current_slide
                == self.ids.fleet_tabs.ids.fleet_ships_tab
            and ce.ship
        ):
            super().on_touch_move(touch)

    def confirm_fleet_change(self, new_fleet: bool=False):
        self.new_fleet = new_fleet
        if ce.fleet:
            change_title = "Clear Current Fleet?" \
                if new_fleet else "Replace Current Fleet?"
            change_button_text = "NEW FLEET" if new_fleet else "LOAD FLEET"
            if not ce.app.confirm_fleet_change_dialog:
                ce.app.confirm_fleet_change_dialog = MDDialog(
                    title=change_title,
                    type="custom",
                    content_cls=ConfirmFleetChangeDialog(),
                    buttons=[
                        MDFlatButton(
                            text="CANCEL", 
                            on_press=lambda x: 
                                ce.app.confirm_fleet_change_dialog.dismiss()
                        ),
                        MDFlatButton(
                            text=change_button_text, 
                            on_press=self._change_fleet
                        )
                    ]
                )
            else:
                ce.app.confirm_fleet_change_dialog.buttons[1].text = \
                    change_button_text
                ce.app.confirm_fleet_change_dialog.title = \
                    change_title
            ce.app.confirm_fleet_change_dialog.open()
        else:
            self._change_fleet()

    def _change_fleet(self,*args):
        try:
            ce.app.confirm_fleet_change_dialog.dismiss()
        except:
            pass
        if self.new_fleet:
            ce.fleet = Fleet('New Fleet')
            self.load_fleet()
        else:
            ce.app.file_manager_open()


class FleetTabs(MDTabs):

    def on_tab_switch(self, *args):
        super().on_tab_switch(*args)
        self.update_cost()
        if ce.fleet:
            self.ids.fleet_name.text=ce.fleet.name

    def update_cost(self, new_fleet: bool = False):
        if ce.fleet:
            if new_fleet:
                self.ids.fleet_cost.text = "Cost: 0"
            else:
                self.ids.fleet_cost.text = f"Cost: {ce.fleet.cost()}"

    def fleet_name_change(self, name: str):
        if not ce.fleet:
            ce.fleet = Fleet(name)
        else:
            ce.fleet.name = name

    def fleet_description_change(self, description: str):
        if not ce.fleet:
            ce.fleet = Fleet('New Fleet')
        ce.fleet.description = description


class FleetOverviewTab(MDStackLayout, MDTabsBase):
    pass


class DeleteShipDialog(MDBoxLayout):
    pass


class FleetShipsTab(MDFloatLayout, MDTabsBase):

    def on_kv_post(self, base_widget):
        super().on_kv_post(base_widget)
        new_ship_list = [
            {
                'text': c,
                'viewclass': 'OneLineListItem',
                'on_release': lambda x=c: self.new_ship(x)
            } for c in ship_classes
        ]
        new_ship_list.append(
            {
                'text': "Load from Template",
                'viewclass': 'OneLineListItem',
                'on_release': lambda: ce.app.file_manager_open(fmtype='ship')
            }
        )
        self.new_ship_menu = MDDropdownMenu(
            caller=self.ids.new_ship_button,
            items=new_ship_list,
            max_height=dp(48*(len(new_ship_list))),
            width_mult=4.5
        )
        manage_ship_list = [
            {
                'text': "Save Ship as Template",
                'viewclass': 'OneLineListItem',
                'on_release': self._save_ship
            },
            {
                'text': "Duplicate Ship",
                'viewclass': 'OneLineListItem',
                'on_release': self.copy_ship
            },
            {
                'text': "Delete Ship",
                'viewclass': 'OneLineListItem',
                'on_release': self._del_ship
            }
        ]
        self.manage_ship_menu = MDDropdownMenu(
            items=manage_ship_list,
            max_height=dp(48*3),
            width_mult=4
        )

    def new_ship(self, shipclass: str):
        if not ce.fleet:
            ce.fleet = Fleet("New Fleet")
        s = ce.fleet.new_ship(shipclass)
        self._add_ship(s)
    
    def copy_ship(self):
        self.manage_ship_menu.dismiss()
        s = ce.fleet.copy_ship(ce.ship_widget.text)
        self._add_ship(s)

    def _add_ship(self, ship: Ship):
        self.ids.ship_list.add_widget(
            ShipListItem(
                text = ship.name,
                secondary_text=ship.hull_type,
                tertiary_text=f"Cost: {ship.cost()}",
                tab=self
            )
        )
    
    def _save_ship(self):
        self.manage_ship_menu.dismiss()
        ce.ship = ce.fleet.get_ship(ce.ship_widget.text)
        ce.app.file_manager_open(fmtype='ship', fmsave=True)
        
    def _del_ship(self):
        self.manage_ship_menu.dismiss()
        ce.ship_widget.delete_confirmation_dialog()
        

class ShipListItem(TouchBehavior, ThreeLineListItem):
    
    __tab: FleetShipsTab = None
    
    def __init__(self, /, tab: FleetShipsTab, **kwargs):
        super().__init__(**kwargs)
        self.lt = False
        if not self.__tab:
            self.__tab = tab
    
    def on_press(self):
        ce.ship_widget = self

    def on_release(self):
        if not self.lt:
            self.load_ship()
        else:
            self.lt = False

    def on_long_touch(self, touch, *args):
        self.lt = True
        self.__tab.manage_ship_menu.caller = self
        self.__tab.manage_ship_menu.open()
        # super().on_long_touch(touch, *args)

    def load_ship(self):
        ce.ship = ce.fleet.get_ship(self.text)
        ce.ship_widget = self
        ce.app.open_screen('ship_screen')

    def delete_ship(self):
        ce.app.ship_delete_dialog.dismiss()
        ce.fleet.remove_ship(self.text)
        ce.ship_widget = None
        self.__tab.ids.ship_list.remove_widget(self)

    def delete_confirmation_dialog(self):
        ce.ship_widget = self
        if not ce.app.ship_delete_dialog:
            ce.app.ship_delete_dialog = MDDialog(
                title="Delete ship?",
                type="custom",
                content_cls=DeleteShipDialog(),
                buttons=[
                    MDFlatButton(
                        text="CANCEL", 
                        on_press=lambda x: ce.app.ship_delete_dialog.dismiss()
                    ),
                    MDFlatButton(
                        text="DELETE", 
                        on_press=lambda x: ce.ship_widget.delete_ship()
                    )
                ]
            )
        ce.app.ship_delete_dialog.open()
