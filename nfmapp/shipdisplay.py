from kivy.metrics import dp
from kivy.uix.scrollview import ScrollView
from kivymd.uix.behaviors import TouchBehavior
from kivymd.uix.dropdownitem import MDDropDownItem
from kivymd.uix.list import OneLineListItem, ThreeLineListItem, TwoLineListItem
from kivymd.uix.menu import MDDropdownMenu

from kivymd.uix.stacklayout import MDStackLayout
from kivymd.uix.tab import MDTabsBase

from nfm import ShipSocket

from . import commonelements as ce


class ShipScreen(ce.NFMScreen):

    prev_screen = 'fleet_screen'

    def on_pre_enter(self, *args):
        socketlist = self.ids.ship_components_tab.ids.components_list
        if ce.ship:
            if ce.socket_widget in socketlist.children:
                for w in socketlist.children:
                    if type(w) is not ce.ListSeparator:
                        if (s:=w.get_socket()).component:
                            if type(w) is EmptySocketListItem:
                                secondary_text, tertiary_text = \
                                    _socketdetails(s)
                                socketlist.add_widget(
                                    SocketListItem(
                                        text=w.text,
                                        secondary_text=secondary_text,
                                        tertiary_text=tertiary_text,
                                        slot_type=s.slot
                                    ),    
                                    index=socketlist.children.index(w)
                                )
                                socketlist.remove_widget(w)
                            else:
                                w.secondary_text, w.tertiary_text \
                                    = _socketdetails(s)
                        elif type(w) is not EmptySocketListItem:
                            socketlist.add_widget(
                                EmptySocketListItem(
                                    secondary_text=\
                                        w.tertiary_text.split(' /')[0],
                                    slot_type=s.slot
                                ),
                                index=socketlist.children.index(w)
                            )
                            socketlist.remove_widget(w)
            else:
                def _addsocketstolist(socketdict: dict):
                    for s in socketdict.values():
                        if (c:=s.component):
                            text = c.name
                            secondary_text, tertiary_text = _socketdetails(s)
                            socketlist.add_widget(
                                SocketListItem(
                                    text=text,
                                    secondary_text=secondary_text,
                                    tertiary_text=tertiary_text,
                                    slot_type = s.slot
                                )
                            ) 
                        else:
                            x,y,z = s.size
                            socketlist.add_widget(
                                EmptySocketListItem(
                                    secondary_text=f"{s.name} - {x}x{y}x{z}",
                                    slot_type = s.slot
                                )
                            )
                ce.socket_widget = None
                socketlist.clear_widgets()
                self.ids.ship_overview_tab.ids.ship_name.text = ce.ship.name
                self.ids.ship_overview_tab.ids.ship_number.text = \
                    str(ce.ship.number)
                socketlist.add_widget(ce.ListSeparator(text="Mounting"))
                _addsocketstolist(ce.ship.mounts)
                socketlist.add_widget(ce.ListSeparator(text="Compartment"))
                _addsocketstolist(ce.ship.compartments)
                socketlist.add_widget(ce.ListSeparator(text="Module"))
                _addsocketstolist(ce.ship.modules)
                tab: ShipOverviewTab = self.ids.ship_overview_tab
                symbol_menu_items = [
                    {
                        'viewclass': 'OneLineListItem',
                        'text': sym,
                        'on_release': lambda x=sym: tab.ship_symbol_change(x)
                    } for sym in ce.ship._symbols
                ]
                ship_symbol: MDDropDownItem = tab.ids.ship_symbol
                tab.symbol_menu = MDDropdownMenu(
                    caller=ship_symbol,
                    items=symbol_menu_items,
                    max_height=dp(48*(len(symbol_menu_items))),
                    width_mult=1.2
                )
                ship_symbol.set_item(ce.ship.symbol)
                # tab.ids.ship_number.x = int(ship_symbol.right) + 100
                # tab.ids.dash.center_x = (ship_symbol.right + 210)/2
            self.update_overview()
        ce.comp = None
        super().on_pre_enter(*args)

    def on_touch_move(self, touch):
        if self.ids.ship_tabs.carousel.current_slide \
            == self.ids.ship_overview_tab:
            super().on_touch_move(touch)

    def update_overview(self):
        i = self.ids.ship_overview_tab.ids
        i.ship_cost.text = f"Cost: {ce.ship.cost()}"
        i.ship_crew.text = \
            f"Crew: {ce.ship.crew_required()}/{ce.ship.crew_complement()}"
        i.ship_power.text = \
            f"Power: {ce.ship.power()}/{ce.ship.power_capacity()} kW"
        ce.ship_widget.tertiary_text = i.ship_cost.text
        self.ids.ship_overview_tab.validate()
    
    def on_pre_leave(self, *args):
        self.ids.ship_overview_tab.symbol_menu.dismiss()
        return super().on_pre_leave(*args)


class ShipOverviewTab(MDStackLayout, MDTabsBase):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.symbol_menu: MDDropdownMenu = None

    def ship_name_change(name: str):
        ce.ship.name = name
        ce.ship_widget.text = name

    def ship_number_change(num: int):
        ce.ship.number = num
        
    def ship_symbol_change(self, symbol: str):
        self.symbol_menu.dismiss()
        ce.ship.symbol = symbol
        self.ids.ship_symbol.set_item(symbol)
        
    def ship_callsign_change(callsign: str):
        ce.ship.callsign = callsign if callsign else None
        
    def validate(self):
        self.ids.validation_errors.clear_widgets()
        errors = ce.ship.validate()
        if errors:
            for error in errors:
                if type(error) is str:
                    self.ids.validation_errors.add_widget(
                        OneLineListItem(text=error)
                    )
                elif type(error) is dict:
                    for k,v in error.items():
                        for e in v:
                            self.ids.validation_errors.add_widget(
                                ErrorListItem(
                                    text=k,
                                    secondary_text=f"{e} - {ce.ship.all_components[e].component}"
                                )
                            )
            self.ids.validation_errors_sv.size_hint_y = None
            self.ids.validation_errors_sv.height = ErrorListItem().height*min(len(errors),2)
            self.ids.validation_errors_sv.scroll_to(
                self.ids.validation_errors.children[-1]
            )
        else:
            self.ids.validation_errors_sv.size_hint_y = 0



class ShipComponentsTab(ScrollView, MDTabsBase):
    pass


class EmptySocketListItem(TwoLineListItem):

    def __init__(self, /, slot_type: str, **kwargs):
        super().__init__(**kwargs)
        self.lt = False
        self.slot_type = slot_type

    def on_release(self):
        if not self.lt:
            self.change_component()
            super(TwoLineListItem, self).on_release()
        else:
            self.lt = False

    def change_component(self):
        self.load_socket()
        ce.app.open_screen('component_select_screen')

    def load_socket(self):
        ce.socket = self.get_socket()
        ce.socket_widget = self

    def get_socket(self):
        return ce.ship.all_components[self.secondary_text.split(' - ')[0]]


class SocketListItem(TouchBehavior, ThreeLineListItem, EmptySocketListItem):

    def on_long_touch(self, touch, *args):
        self.lt = True
        self.load_socket()
        ce.comp = ce.socket.component
        ce.app.open_screen('component_details_screen')
        super().on_long_touch(touch, *args)

    def get_socket(self):
        return ce.ship.all_components[self.tertiary_text.split(' - ')[0]]


class ErrorListItem(TwoLineListItem):
    
    def on_release(self):
        ce.socket = ce.ship.all_components[self.secondary_text.split(' - ')[0]]
        ce.comp = ce.socket.component
        ce.app.open_screen('component_details_screen')
        super().on_release()


def _socketdetails(socket: ShipSocket) -> tuple:
    secondary_text = f"{socket.component.type} ({socket.cost()} pts)"
    if p:=socket.power_provided() :
        secondary_text += f": Provides {p} kW"
    if c:=socket.crew_provided():
        secondary_text += f": Supports {c} Crew"
    if (cap:= socket.get_capacity()):
        secondary_text += f": {cap[0]}: {cap[1]}/{cap[2]}"
    if socket.weapon_group_type:
        group = wg if (wg:=socket.weapon_group) else "<Unassigned>"
        secondary_text += f": {group}"
    x,y,z = socket.size
    tertiary_text = f"{socket.name} - {x}x{y}x{z}"
    if p:=socket.power():
        tertiary_text += f" / {p} kW"
    if c:=socket.crew():
        tertiary_text += f" / {c} Crew"
    return secondary_text,tertiary_text
