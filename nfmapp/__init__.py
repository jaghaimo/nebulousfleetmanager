from glob import glob

from kivy.lang.builder import Builder

from . import (commonelements, fleetdisplay, shipdisplay, componentdisplay, 
    ammodisplay)

def load():
    commonelements.app.kv_directory = 'nfmapp'
    for kv in glob('nfmapp/*.kv'):
            if not kv.endswith('main.kv'):
                Builder.load_file(kv)